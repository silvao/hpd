#!/Library/Frameworks/Python.framework/Versions/3.6/bin/python3.6


def com_break():
    for c in range(1,6):
        if c == 4:
            break
        print(c)

def com_continue():
    for c in range(1,6):
        if c == 4:
            continue
        print(c)

com_break()
com_continue()