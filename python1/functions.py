#!/Library/Frameworks/Python.framework/Versions/3.6/bin/python3.6

def pause():
    input("\n\nPressione enter para continuar...\n\n")

def mensagemFim():
    print("Valeu por usar esse programa")
    print("VAIII")

def imprimaTresLinhas():
    for i in range(1,4):
        print('Esta eh a linha ' + str(i))

def imprimeNoveLinhas():
    for i in range(1,4):
        imprimaTresLinhas()

def mensagemInicio():
    print("Este programa eh somente para mostrar como funciona o uso de functions")
    pause()

def linhaBranco():
    print()

def limpaTela():
    for i in range(1,26):
        linhaBranco()

mensagemInicio()
