#!/Library/Frameworks/Python.framework/Versions/3.6/bin/python3.6
import docker
import argparse
from datetime import datetime


def logando(mensagem, e, logfile='docker-cli.log'):
    data_atual = datetime.now().strftime('%d/%m/%Y %H:%M')
    with open('docker-cli.log', 'a') as log:
        texto = "%s \t %s \t %s \n" %(data_atual,mensagem,str(e))
        log.write(texto)

def conectar():
    client = docker.from_env() 
    return client

def listar_todos():
    client = conectar()
    get_all = client.containers.list(all)
    return get_all

def listar_imagem():
   client = conectar()
   get_all = listar_todos()
   for cada_container in get_all:
       conectando = client.containers.get(cada_container.id)
       print("O container %s utiliza a imagem %s e esta rodando o comando %s " %(conectando.short_id, conectando.attrs['Config']['Image'], conectando.attrs['Config']['Cmd']))

def criar_imagem(imagem, comando):
    try:
        client = conectar()
        executando = client.containers.run(imagem,comando)
        print(executando)
    except docker.errors.ImageNotFound as error:
        logando("Erro! Essa imagem nao existe!",error)
        #print("Erro! Essa imagem nao existe!\n", error)    
    except docker.errors.NotFound as error:
        logando("Erro! Esse comando nao existe!",error)
       # print("Erro! Esse comando nao existe!\n", error)    

def procurar_imagem(imagem_busca):
    client = conectar()
    get_all = listar_todos()
    for cada_container in get_all:
       conectando = client.containers.get(cada_container.id)
       imagem_container = conectando.attrs['Config']['Image']
       if str(imagem_busca).lower() == str(imagem_container).lower():
           print("O container %s utiliza a imagem %s " %(conectando.short_id, conectando.attrs['Config']['Image']))
       
def remover_imagem():
    client = conectar()
    get_all = listar_todos() 
    for cada_container in get_all:
        conectando = client.containers.get(cada_container.id)
        host_binding = conectando.attrs['HostConfig']['PortBindings']
        if isinstance(host_binding,dict):
            for porta,porta1 in host_binding.items():
                porta1 = str(porta1)
                porta2 = ''.join(filter(str.isdigit, porta1))
                if int(porta2) < 1024:
                    print("O container %s utiliza uma porta abaixo de 1024 e sera excluido " %conectando.short_id)
                    removendo = cada_container.remove(force=True)
                    
        
 


def criar(args):
     criar_imagem(args.imagem,args.comando)

def listar():
     listar_imagem()

def procurar(args):
     procurar_imagem(args.imagem)

def remover(args):
    remover_imagem(args.imagem)

parser = argparse.ArgumentParser(description="docker-cli feito durante a aula.")
subparser = parser.add_subparsers()

criar_opt = subparser.add_parser('criar')
criar_opt.add_argument('--imagem', required=True)
criar_opt.add_argument('--comando', required=True)
criar_opt.set_defaults(func=criar)


listar_opt = subparser.add_parser('listar')
listar_opt.add_argument('--imagem',required=False)
listar_opt.set_defaults(func=listar)


procurar_opt = subparser.add_parser('procurar')
procurar_opt.add_argument('--imagem',required=True)
procurar_opt.set_defaults(func=procurar)


remover_opt = subparser.add_parser('remover')
remover_opt.add_argument('--imagem',required=True)
remover_opt.set_defaults(func=remover)

cmd = parser.parse_args()
cmd.func(cmd)


#client = docker.from_env()
#client.containers.run('nginx', detach=True)
#criar("alpine","uname -a")
#listar()
#procurar("alpine")
#procurar("nginx")
#remover()