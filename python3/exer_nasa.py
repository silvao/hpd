
#!/Library/Frameworks/Python.framework/Versions/3.6/bin/python3.6
import requests
import docker
from datetime import datetime

def conectar():
    client = docker.from_env() 
    return client


def logando(mensagem, e, logfile='docker-cli.log'):
    data_atual = datetime.now().strftime('%d/%m/%Y %H:%M')
    with open('docker-cli.log', 'a') as log:
        texto = "%s \t %s \t %s \n" %(data_atual,mensagem,str(e))
        log.write(texto)

def criar_container(imagem,nome_container='myContainer2'):
    try:
        client = conectar()
        executando = client.containers.create(imagem,name=nome_container)
        logando("Criando container",executando)
    except docker.errors.ImageNotFound as error:
        logando("Erro! Imagem não localizada!",error)
    except docker.errors.NotFound as error:
        logando("Erro! Comando não localizado!",error)
    except docker.errors.APIError as error:
         logando("Erro! Ao criar container!",error) 


def criar(nome_container):
     criar_container("alpine",nome_container)

def consumir_lista(listagem):
    for pessoa in listagem:
        nome = pessoa['name']
        nome_tratado = nome.replace(" ","-")
        criar(nome_tratado)
        #print(pessoa['name'])


def consome_api():
    url = "http://api.open-notify.org/astros.json"
    resposta = requests.get(url)
    logando("Status retorno API", resposta.status_code)
    dados = resposta.json()
    #print(dados)
    total = dados['number']
    #print(total)
    lista_pessoas = dados['people']
    #print("No momento temos %s pessoa(s) no espaço. São elas: " %total)
    consumir_lista(lista_pessoas)
    


consome_api()




#print(lista_pessoas)