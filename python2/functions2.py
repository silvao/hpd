#!/Library/Frameworks/Python.framework/Versions/3.6/bin/python3.6
import subprocess
import argparse

def listar_arquivos(myDir):
    print("Dir2: " + myDir)
    subprocess.call(['ls','-ll'], cwd=myDir)  

def criar_diretorios(args):
    mydir = args.nome
    subprocess.call(['mkdir', mydir])
    return mydir

def criar_arquivos(myDir):
    print("Dir: " + myDir)
    for i in range(1,40):
        nome = "arquivo" + str(i) + ".txt"
        comando = "touch " + nome
        subprocess.call(['touch',nome], cwd=myDir)

def criar(args):
     dir = criar_diretorios(args)
     criar_arquivos(dir)


def listar(args):
     listar_arquivos(args.diretorio)

parser = argparse.ArgumentParser(description="Comando para criar e listar diretorios durante a aula.")
subparser = parser.add_subparsers()

criar_dir = subparser.add_parser('criar')
criar_dir.add_argument('--nome', required=True)
criar_dir.set_defaults(func=criar)


# create the parser for the "bar" command
listar_dir = subparser.add_parser('listar')
listar_dir.add_argument('--diretorio')
listar_dir.set_defaults(func=listar)



cmd = parser.parse_args()
cmd.func(cmd)



#retorno = cria_diretorios()
#print(retorno)
#cria_arquivos(retorno)
#lista_arquivos(retorno)